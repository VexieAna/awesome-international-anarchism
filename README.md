# Anarchist groups, anarcho syndicalist unions & revolutionary unions

Awesome list of active anarchist (and alike) organisations.  
Submit your additions/suggestions/whatever via a new PR/MR or reach out [aga@agamsterdam.org](mailto:aga@agamsterdam.org).

## Index

+ [Argentina](#argentina)
+ [Australia](#australia)
+ [Austria](#austria)
+ [Bangladesh](#bangladesh)
+ [Belarus](#belarus)
+ [Belgium](#belgium)
+ [Brazil](#brazil)
+ [Bulgaria](#bulgaria)
+ [Canada](#canada)
+ [Croatia](#croatia)
+ [Czech republic](#czech-republic)
+ [Cyprus](#cyprus)
+ [Denmark](#denmark)
+ [Ecuador](#ecuador)
+ [Estonia](#estonia)
+ [France](#france)
+ [Germany](#germany)
+ [Greece](#greece)
+ [Iceland](#iceland)
+ [India](#india)
+ [Indonesia](#indonesia)
+ [Latvia](#latvia)
+ [Iran](#iran)
+ [Ireland](#ireland)
+ [Italy](#italy)
+ [Mexico](#mexico)
+ [Netherlands](#netherlands)
+ [Norway](#norway)
+ [Philippines](#philippines)
+ [Poland](#poland)
+ [Portugal](#portugal)
+ [Romania](#romania)
+ [Russia](#russia)
+ [Serbia](#serbia)
+ [Slovakia](#slovakia)
+ [Spain](#spain)
+ [Sweden](#sweden)
+ [Turkey](#turkey)
+ [Ukrania](#ukrania)
+ [Uruguay](#uruguay)
+ [United Kingdom](#united-Kingdom)
+ [United States of America](#united-states-of-america)
+ [World wide](#world-wide)
+ [Others](#others)

---

## Countries

---
### Argentina

#### Federacion libertaria argentina

http://federacionlibertariaargentina.org/home.html


#### Red libertaria de Buenos Aires

http://www.redlibertaria.com.ar/


---
### Australia

#### Anarcho-Syndicalist Federation

http://asf-iwa.org.au/


#### IWW australia

http://iww.org.au/


#### Collective Action

http://www.collectiveaction.org.au


---
### Austria

#### Wiener ArbeiterInnen Syndikat

https://wiensyndikat.wordpress.com/

#### IWW Austria 

https://iww.or.at/


---
### Bangladesh

#### Bangladesh Anarcho-Syndicalist Federation

https://www.bangladeshasf.org/


---
### Belarus

#### Belarus Anarchist Black Cross

https://abc-belarus.org


---
### Belgium

#### Leuven Anarchistische Groep

https://leuvenag.noblogs.org/


#### Anarchistisch Kollektief Gent

https://anarchistischkollektief.wordpress.com/


#### Anarchistisch Collectief Antwerpen

https://facebook.com/anarchistischcollectiefantwerpen


---
### Brazil

#### Anarkio

https://anarkio.net/index.php/contato/


#### Iniciativa Federalista Anarquista no Brasil

https://ligarj.wordpress.com/category/iniciativa-federalista-anarquista-brasil/


#### Federação das Organizações Sindicalistas Revolucionárias do Brasil

https://lutafob.wordpress.com/


#### Federação Anarquista Gaúcha / Fórum do Anarquismo Organizado (FAO)

http://www.vermelhoenegro.co.cc/


#### Federação Anarquista do Rio de Janeiro / FAO 

http://www.farj.org/


#### C.O.B

http://cob-ait.net/


---
### Bulgaria

#### Autonomous Workers' Confederation (ARK) 

http://avtonomna.com/


#### Federation of Anarchists in Bulgaria

https://www.anarchy.bg/

  
#### Confederation of Independent Trade Unions of Bulgaria  (KNSB/CITUB)

http://www.knsb-bg.org/


#### ARS (AIT)

http://arsindikat.org/


---
### Canada

#### IWW Canada

https://industrialworker.org/


#### Common cause

http://linchpin.ca/


#### BCBlackOut - Social War Against Industrial Expansion 

https://bcblackout.wordpress.com/


---
### Croatia

#### MASA (AIT)

https://masa-hr.org/

---
### Czech republic

#### Socialista solidarita

https://organizace.socsol.cz/


#### Anarchistická federace 

https://www.afed.cz/


#### Kollektiv 115

https://k115.org/


---
### Cyprus

#### Syspirosi Atakton

https://syspirosiatakton.org/


---
### Denmark

#### Libertære Socialister

http://libsoc.dk/


---
### Ecuador

#### Chisque anarquista

http://chasquianarquista.blogspot.com/


---
### Estonia

---
### France

#### Fédération Anarchiste 

https://federation-anarchiste.org/


#### Union communiste libertaire 

https://unioncommunistelibertaire.org/


#### Section carrément anti-Le Pen 

http://nopasaran.samizdat.net/


#### CNT 

http://www.cnt-f.org/


#### Attaque

https://attaque.noblogs.org


#### Anarchist Bure Cross

https://anarchistburecross.noblogs.org/


---
### Germany

#### IWW Germany

https://www.wobblies.org


#### FAU 

https://www.fau.org/


#### Föderation deutschsprachiger AnarchistInnen/ Federation of German speaking Anarchists

https://fda-ifa.org/


#### Gefangenen Gewerkschaft-Bundesweite Organisation

https://ggbo.de/


#### …ums Ganze!

https://www.umsganze.org/


#### Efya

https://eyfa.org/


---
### Greece

#### Rocinante

https://rocinante.gr/


#### Libertarian Syndicalist Union - ESE (CIT)

https://ese.espiv.net/ 


#### Workers Grassroot Federation

https://ergova.wordpress.com/ 


#### Union of waitors/waitresses/cooks and workers in restaurants (Athens)

http://somateioserbitoronmageiron.blogspot.com/ 


#### Union of waitors/waitresses/cooks and workers in restaurants (Thessaloniki)

https://ssmthess.espivblogs.net/ 


#### Grassroot Assembly of Motorbike Workers

http://sveod.gr/ 


#### Union of Employees in Book – Paper – Digital Media industry (Attiki)

https://bookworker.wordpress.com/ 


#### Grassroot Union of Employees in NGOs

https://svemko.espivblogs.net/ 


#### Grassroot Union of Employees in the mental health and social providence field

https://svepsykoi.espivblogs.net/ 


#### Union of Translators – Correctors

http://www.smed.gr/ 


#### National Union of Employees in Vodafone

http://www.pasevodafone.gr/ 


#### National Union of Employees in Wind

http://pasetim.com/ 


#### Assembly of employees/unemployed/students in media

http://katalipsiesiea.blogspot.com/ 


#### Assembly of employees/unemployed in dial/call lines

https://proledialers.espivblogs.net/ 


#### “Orthostasia” employees in shops

https://orthostasia.wordpress.com/ 


#### “Class Front” Initiative of employees in public transport

https://taxikometopo.wordpress.com/ 


#### Αναρχική Πολιτική Οργάνωση / Anarchist Political Organisation

http://apo.squathost.com/


#### Αντιεξουσιαστική Κίνηση / Anti-Authoritarian Movement

https://www.antiauthoritarian.gr/


---
### Iceland

#### IWW Iceland

https://iwwisland.org/


---
### India

#### Muktivadi Ekta Morcha

https://muktivadi.blackblogs.org/


---
### Indonesia

#### Persaudaraan Pekerja Anarko Sindikalis

https://ppasonline.wordpress.com/


---
### Iran

#### Anarchist Union of Afghanistan & Iran

http://asranarshism.com/


---
### Ireland

#### Workers Solidarity Movement 

http://www.wsm.ie/


#### Solidarity federation 

http://www.solfed.org.uk/local/belfast


#### IWW Ireland

https://onebigunion.ie/


#### Anarchist Black Cross Ireland

https://abcireland.wordpress.com


#### Derry Anarchist Collective

https://derryanarchists.blogspot.com/


---
### Italy

#### Federation of Anarchist Communists 

http://www.fdca.it/


#### CIB UNICOBAS (Confederazione Italiana di Base) 

https://www.unicobas.org/


#### Federazione Anarchica Italiana 

http://www.federazioneanarchica.org/


#### umanita nova 

https://umanitanova.org/


#### Union of Anarchist Communist of Italy 

http://www.ucadi.org/


#### Unione Sindacale Italiana 

https://www.unionesindacaleitaliana.eu/


#### USI

https://usi-cit.org/


#### IWW Italy

https://iwwita.it/


---
### Latvia

---
### Mexico

#### Federation of Anarchists in Mexico / Federación Anarquista de México

http://federacionanarquistademexico.org/


#### Voces Oaxaqueñas Construyendo Autonomía y Libertad (Mexico)

http://vocal.saltoscuanticos.org/


---
### Netherlands

#### Anarchistische Groep Amsterdam

http://www.agamsterdam.org/


#### Anarchistische Groep Nijmegen

https://www.anarchistischegroepnijmegen.nl/


#### Anarchist Black Cross (ABC) Nijmegen

https://abcnijmegen.wordpress.com/


#### Autonomen Den Haag

https://autonomendenhaag.wordpress.com/


#### Brabantse Anarchistische Kring

https://brabantseak.noblogs.org/


#### Doorbraak

http://www.doorbraak.eu/


#### Groenfront

https://www.groenfront.nl/


#### Autonomen Brabant

http://autonomenbrabant.nl/


#### Vloerwerk

https://vloerwerk.org/


#### Fairwork

https://www.fairwork.nu/en/homepage/


#### Vrije Bond

http://www.vrijebond.org


---
### Norway

#### Motmakt (Norway)

http://motmakt.no/


#### NSF

http://www.nsf-iaa.org/


---
### Philippines

#### Bandilang Itim

https://bandilangitim.noblogs.org


---
### Poland

#### ZSP (AIT)

http://zsp.net.pl


#### Inicjatywa Pracownicza / Workers Initiative (CIT) 

https://www.ozzip.pl/


---
### Portugal

#### AIT-Secção Portuguesa

http://ait-sp.blogspot.com/


#### Guilhotina

https://guilhotina.info/en/


---
### Romania

#### Ravna

https://iasromania.wordpress.com/


#### Baldovin concept

http://baldovinconcept.blogspot.com/


#### Dreptul la Oras(DO) 

http://dreptullaorastimisoara.com/


#### E-Romnja (Rroma minority related)

www.e-romnja.ro


---
### Russia

#### Confederation of Revolutionary Anarcho-Syndicalists 

https://aitrus.info/


#### Anarchist Black Cross Russia

https://avtonom.org


---
### Serbia 

#### ISA (AIT)


#### ASI-MUR

https://inicijativa.org/tiki/tiki-index.php?page=ASI+English


---
### Slovakia

#### Priama Akcia (AIT)

https://www.priamaakcia.sk/


---
### Spain

#### CNT 

https://www.cnt-ait.org/


#### CGT

http://cgt.org.es/


#### Solidaridad Obrera

https://solidaridadobrera.org/


#### Mierda jobs

https://twitter.com/JobsMierda


---
### Sweden

#### SAC 

https://www.sac.se/en/


#### Swedish Anarcho-syndicalist Youth Federation 

www.suf.cc


#### OLS 

https://orestadls.wordpress.com/


#### Gatorna

https://gatorna.info/about/


---
### Turkey

#### IWW Turkey

https://orestadls.wordpress.com/


#### Anarsist Federasyon.

https://anarsistfederasyon.org/


#### Devrimci Anarşist Faaliyet

https://anarsistfaaliyet.org/


---
### Switzerland

#### IWW Switzerland

https://www.wobblies.org/


#### Libertäre Aktion Winterthur

http://libertaere-aktion.ch/


#### Organisation Socialiste Libertaire

http://www.rebellion.ch/


---
### Uruguay

#### Colectivo Socialista Libertaria

http://periodicorojoynegro.blogspot.com/


---
### Ukrania

#### Autonoms Workers Union 

Big change dont exist anymore, because of civil war.


#### Borotba

http://www.borotba.su/


---
### United Kingdom

#### IWW uk,, Ierland, 

https://iww.org.uk/


#### Anarchist Federation UK

http://afed.org.uk/


#### Class War 

https://classwar.uk/


#### Freedompress 

https://freedompress.org.uk/


#### Solidarity federation 

http://www.solfed.org.uk/


#### Alternative Bristol

https://alternativebristol.com/


#### Anarchist Action Network

https://network23.org


#### Workers wild west

https://workerswildwest.wordpress.com/


#### Angry workers of the world

https://www.angryworkers.org


#### Antifascist Network

https://antifascistnetwork.org


#### Brighton Antifascists

https://brightonantifascists.com


#### Brighton Solidarity Federation

http://www.brightonsolfed.org.uk


#### Cautiously pessimistic

https://nothingiseverlost.wordpress.com


#### Anarchist Federation Glasgow

https://glasgowanarchists.wordpress.com/


#### Green Anticapitalist Media

https://greenanticapitalist.org


#### Haringey solidarity group

http://www.haringey.org.uk/content/


#### IWW Bristol

https://iww.org.uk/bristol/


#### IWW Scotland

https://iwwscotland.wordpress.com/


---
### United States of America

#### Black Rose/Rosa Negra

https://blackrosefed.org/locals/


#### Youth liberation

https://youthliberation.noblogs.org/


#### Youth Liberation Front

https://pnwylf.noblogs.org/chapters/


#### Workers Solidarity Alliance

https://workersolidarity.org/


#### A World Without Police

http://aworldwithoutpolice.org/


#### Black Autonomy Action

https://blackautonomynetwork.noblogs.org/


#### The Center for a Stateless Society

https://c4ss.org/about


#### Central Oregon Antifascism

https://centraloregonantifascist.noblogs.org/


#### Central PA Antifa

https://centralpaantifascist.wordpress.com


#### Food Not Bombs Chehalis River

https://crfnb.noblogs.org/post/2020/12/08/our-first-successful-meal/


#### Chehalis River Mutual Aid

https://chehalisrivermutualaid.noblogs.org


#### CVAntifa

https://cvantifa.noblogs.org


#### FireStorm on fascists

https://firestormonfash.noblogs.org/


#### Ft. Lauderdale Youth Liberation Front

https://flaylf.noblogs.org/a-guide-to-documenting-protests/


#### CrimethInc

https://crimethinc.com/about


#### Libertarian Socialist Caucus

https://dsa-lsc.org/


#### Haleyville Anti-Rapist/Anti-Fascist Action

https://haleyvilleara.blackblogs.org


#### IdaVox

http://idavox.com


#### It's going down

https://itsgoingdown.org


---
### World wide

#### ICL- CIT

https://www.icl-cit.org/
https://www.iclcit.org/


#### AIT

https://iwa-ait.org/


#### IFA

https://i-f-a.org/


#### IWW

https://iww.org/directory/


#### Anarchist Black Cross Federation

https://www.abcf.net


#### All AIT IWA groups and frends group

https://iwa-ait.org/content/addresses


#### Solidarity International

https://solidarity.international


#### Global May Day

https://globalmayday.net


#### Indigenous action

https://www.indigenousaction.org


#### Indigenous Anarchist Federation

https://iaf-fai.org


#### Indigenous mutual aid

https://www.indigenousmutualaid.org/


#### International Anti-Fascist Defence Fund

https://intlantifadefence.wordpress.com/


#### International Labour Network of Solidarity and Struggles

http://www.laboursolidarity.org


#### IWW Washington D.C.

http://www.dciww.org/


#### IWW Detroit

https://www.iwwdetroitgmb.net/


#### IWW New Jersey

https://newjerseyiww.org


#### IWW Portland

https://portlandiww.org/


---
### Others

The equivalent to AIT but non-anarchist based: https://www.etuc.org/en/national-trade-union-confederations-list-member-organisations  


---
### Bibliography

(And yes, wikipedia counts)  
+ https://en.wikipedia.org/wiki/Anarcho-syndicalism  
+ https://zabalaza.net/organise/the-anarkismo-network/  
+ http://anarkismo.net/about_us  
+ https://www.anarchisme.nl/organisaties  
+ https://www.reddit.com/r/Anarchism/comments/kshso6/questions_about_activism/gigpsyz/?utm_source=reddit&utm_medium=web2x&context=3  
+ https://iwa-ait.org/content/addresses  
+ [-] https://www.liquisearch.com/list_of_anarchist_organizations  
+ [-] https://www.anarchistfederation.net/sources-list/  (up to Komun Academy (Kurdish)  )
+ [-] https://solidarity.international/index.php/abc-groups-around-the-world/  
+ [-] https://brightonantifascists.com/links/
+ [-] https://nothingiseverlost.wordpress.com (list on the right menu)
+ [-] https://bandilangitim.noblogs.org/friends/
+ [-] https://eyfa.org/how-we-work/network/members-friends/